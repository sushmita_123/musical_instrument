from django.contrib import admin
from .models  import Manager,Coache,Genre,Student,Payment

# Register your models here.
admin.site.register(Manager)
admin.site.register(Coache)
admin.site.register(Genre)
admin.site.register(Student)
admin.site.register(Payment)

