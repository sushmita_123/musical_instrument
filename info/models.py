from django.db import models
class Manager(models.Model):
    title=models.CharField(max_length=100,unique=True,null=True,blank=True)
    time_detail=models.CharField(max_length=200,null=True)


    def __str__(self):
        return self.title

class Coache(models.Model):
    title = models.CharField(max_length=100,null=True,blank=True)
    description=models.TextField(null=True,blank=True)
    image=models.ImageField(upload_to='info/',null=True,blank=True)
    contact_number=models.CharField(max_length=15,unique=True)
    manager=models.ForeignKey(Manager,on_delete=models.CASCADE)

    def __str__(self):
        return self.title
    class meta():
        db_table="info"


class Genre(models.Model):
    title = models.CharField(max_length=100, unique=True, null=True)
    image=models.ImageField(upload_to='info/',null=True,blank=True)
    type=models.TextField(null=True,blank="True")
    coache=models.ForeignKey(Coache,on_delete=models.CASCADE)

    def __str__(self):
        return self.title
    class meta():
        db_table="info"


class Student(models.Model):
    title=models.CharField(max_length=100,unique=True)
    email=models.TextField(null=True,blank=True)
    coache=models.ForeignKey(Coache,on_delete=models.CASCADE)
    genre = models.ForeignKey(Genre, on_delete=models.CASCADE)

    def __str__(self):
        return self.title
    class meta():
        db_table="info"

class Payment(models.Model):
    name=models.CharField(max_length=100,unique=True)
    pay_info=models.CharField(max_length=100,null=True,blank=True)

    def __str__(self):
        return self.title
    class meta():
        db_table="info"

