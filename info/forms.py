from django import forms
from .models  import Coache,Manager,Genre,Payment

class ManagerForm(forms.ModelForm):
    title=forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','placeholder':'title'}))
    description=forms.CharField(widget=forms.Textarea(attrs={'class':'form-control'}))
    Coache=forms.ModelChoiceField(queryset=Coache.objects.all(),widget=forms.Select(attrs={'class':'form-control'}))
    Genre=forms.ModelChoiceField(queryset=Genre.objects.all(),widget=forms.Select(attrs={'class':'form-control'}))
    class Meta:
        model =Manager
        fields ='__all__'


class PaymentForm(forms.ModelForm):
    name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'title'}))
    pay_info=forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Payment Type'}))

    class Meta:
        model = Payment
        fields = '__all__'