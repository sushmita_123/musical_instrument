from django.shortcuts import render,HttpResponse,redirect,get_object_or_404
from info.models import Manager,Genre,Payment
from django.contrib.auth.models import User
from django.contrib.auth import authenticate,login,logout
from django.contrib import messages
from info.forms import ManagerForm,PaymentForm
from django.contrib.auth.decorators import login_required
def about(request):
    return render(request,'about.html')

def views_more(request,id):
    data = get_object_or_404(Manager, pk=id)
    context = {
        'manager': data
    }
    return render(request, 'views_more.html', context)


def home(request):
        data=Manager.objects.all()
        context={
            'manager':data
        }
        return render(request,'home.html',context)

def signin(request):
    if request.method=='GET':
        return render(request, 'login.html')

    else:
        u=request.POST['username']
        p=request.POST['pass1']
        user=authenticate(username=u,password=p)
        if user is not None:
            login(request,user)
            return redirect('dashboard')

        else:
            messages.add_message(request, messages.ERROR, "password does not match")
            return redirect('signin')

@login_required(login_url='signin')
def dashboard(request):
    data=Manager.objects.all() [::-1]
    context={
        'manager':data
    }
    return render(request,'dashboard.html',context)

def payment_detail(request):
    data=Payment.objects.all() [::-1]
    context={
        'payment':data
    }
    return render(request,'Payment_detail.html',context)



def signup(request):
    if request.method=='GET':
        return render(request,'signup.html')
    else:
        u=request.POST['username']
        e=request.POST['email']
        p1=request.POST['pass1']
        p2=request.POST['pass2']
        if p1 == p2:
            try:
                u = User(username=u, email=e)
                u.set_password(p1)
                u.save()
            except:
                messages.add_message(request, messages.ERROR, "username already exist")
                return redirect('signup')
            messages.add_message(request, messages.SUCCESS, "password and conf password  match")
            return redirect('signin')
        else:
            messages.add_message(request, messages.ERROR, "password and conf password doesn't match")
            return redirect('signup')


def signout(request):
    logout(request)
    return redirect('signin')
def create_post(request):
    form=ManagerForm(request.POST or None,request.FILES or None )
    if form.is_valid():
        form.save()
        messages.add_message(request,messages.SUCCESS,"Created successfully")
        return redirect('dashboard')
    context ={
        'form':form
    }
    return render(request,'create_post.html',context)

def editpost(request,id):
    data=Manager.objects.get(pk=id)
    form=ManagerForm(request.POST or None,request.FILES or None,instance=data)
    if form.is_valid():
        form.save()
        messages.add_message(request,messages.SUCCESS,"update successfully")
        return redirect('dashboard')
    context={
        'form':form
    }
    return render(request,'edit_post.html',context)


def deletepost(request,id):
    b=Manager.objects.get(pk=id)
    b.delete()
    messages.add_message(request,messages.SUCCESS,"successfully deleted")
    return redirect('dashboard')


def about(request):
    return render(request,'about.html')

def genre(request):
    return render(request,'genre.html')

def instrument(request):
    return render(request,'instrument.html')


def payment(request):
    form = PaymentForm(request.POST or None, request.FILES or None)
    if form.is_valid():
        form.save()
        messages.add_message(request, messages.SUCCESS, "Created successfully")
        return redirect('dashboard')
    context = {
        'form': form
    }
    return render(request, 'payment.html', context)
